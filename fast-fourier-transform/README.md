# Fast Fourier Transform
An implementation of Tukey's FFT.

# Links
* [DSP Guide](http://www.dspguide.com/ch12/2.htm)
* [FFT Walkthrough](http://www.alwayslearn.com/dft%20and%20fft%20tutorial/DFTandFFT_FFT_Overview.html)
* [Inverse FFT](http://adamsiembida.com/how-to-compute-the-ifft-using-only-the-forward-fft/)
* [Mathematics of the DFT](https://www.dsprelated.com/freebooks/mdft/)
* [Princeton CS](http://introcs.cs.princeton.edu/java/97data/)
